//ARRAYS AND INDICES

/*Arrays are used to store multiple related values in a single variable
Arrays are declared using square brackets '[]', also known as an Array Literal
*/

let grades = [98.5, 94.3, 89.2, 90.1]

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

//READING FROM AN ARRAY

//console.log(computerBrands[5])

//MUTATOR METHODS

//pop() - Removes an array's last element and returns it.

// console.log(grades.pop())

//push() - Adds one or more elements at the end of an array and returns the array's length.

// grades.push(85.5)

//length - Check the number of elements within an array

// console.log(grades.length)

//shift() - Removes an element at the beginning of an array and returns it.

// console.log(grades.shift())

//unshift() - Add one or more elements at the beginning of an array and returns the array's length.

// console.log(grades.unshift(77.7, 88.8))

//splice() - Can add, remove, or add and remove simultaneously

//syntax: .splice(index number, number of items to remove, items to add)

// console.log(tasks)

//ADD WITH SPLICE

// tasks.splice(3, 0, "eat lunch")

// //REMOVE WITH SPLICE

// tasks.splice(2, 1)

// //ADD AND REMOVE WITH SPLICE

// tasks.splice(3, 2, "code")

// //reverse() - Reverses the order of the elements

// // tasks.reverse()

// //sort() - Rearranges the array elements in alphanumeric order.

// tasks.sort()

//ACCESSOR METHODS

//concat() - Combines two arrays.

let a = [1, 2, 3, 4, 5]
let b = [6, 7, 8, 9, 10]

let ab = a.concat(b);

// console.log(ab)

//join() - Converts the elements of an array into a new string

// console.log(ab.join(" and "))

//slice() - Copies a portion of an array to a new array

//syntax: slice(index number to start, index number to end -1) **up to but NOT including the second index number

let abSlice = ab.slice(0, 2)

// console.log(abSlice)

//indexOf() - Returns the index number of the first instance of an element

// console.log(computerBrands.indexOf('Alienware'))

//lastIndexOf() - Returns the index number of the last instance of an element

// console.log(computerBrands.lastIndexOf('Neo'))

//ITERATION METHODS (loops)

let tasks = [
	'shower',
	'eat breakfast',
	'go to work',
	'go home',
	'go to sleep'
]

//forEach()

// tasks.forEach(function(task) {
// 	console.log(task)
// })

//map() - creates a new array populated with the results of running a function on each element of the mapped array

let numbers = [1, 2, 3, 4, 5]

let doubledNumbers = numbers.map(function(number) {
	return number * 2
})

// console.log(doubledNumbers)

//filter() - creates a new array with all elements that pass the test implemented by the provided function

let words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present']

let wordsResult = words.filter(function(word){
	return word.length > 6;
})

// console.log(wordsResult)

//TWO-DIMENSIONAL ARRAYS

let numberArrays = [
	[1.1, 1.2, 1.3, 1.4],
	[2.1, 2.2, 2.3, 2.4],
	[3.1, 3.2, 3.3, 3.4]
]

// console.log(numberArrays[2][0])

//SPREAD OPERATOR (...) - "Spreads" the elements of an array anywhere where elements are expected

//Add elements of an existing array to a new array

// let arrayA = [1, 2, 3]
// let arrayB = ["a", ...arrayA, "b", "c"]

//Copy arrays

// let arrayA = [1, 2, 3]
// let arrayB = [...arrayA]

// arrayB.push(4)

// console.log(arrayA)
// console.log(arrayB)

//Concatenate arrays

let arrayA = [1, 2, 3]
let arrayB = [4, 5, 6]

let arrayC = [...arrayA, ...arrayB]

console.log(arrayC)

/*
Activity:

In WDC028-S12/A1, do the following:

Create an array named students with at least 3 unique names

Create 3 different functions with the following instructions:
	-Add a new student to the students array
	-Log in the console all the students one by one
	-Count the number of students and display the results in the console

Stretch goal:
	-Create another function which filters a student with a specific keyword using the filter method and the variable.includes(keyword) function.
*/